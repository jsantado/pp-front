/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        "main": "#32383B",
        "secondary": "#BBC3CE",
        "additional": "#B9A04D",
        "disabled": "#717275",
        "error": "#FB0900"
      }
    },
  },
  plugins: [],
}
