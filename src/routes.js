
import React from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';
import Wrapper from './components/Wrapper';
import { AdminBlogs, Home, Login, Translations } from './screens';

const AppRoutes = () => (
    <Routes>
        <Route index element={<Wrapper><Home /></Wrapper>} />
        <Route path="/login" element={<Wrapper><Login /></Wrapper>} />
        <Route path="*" element={<Navigate to="/" />} />
        <Route path="/admin/translations" element={<Wrapper><Translations /></Wrapper>} />
        <Route path="/admin/blogs" element={<Wrapper><AdminBlogs /></Wrapper>} />
    </Routes>
);

export default AppRoutes;