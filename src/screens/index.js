import Home from './Home';
import Login from './Login';
import Translations from './Translations';
import AdminBlogs from './AdminBlogs';

export {
    Home,
    Login,
    Translations,
    AdminBlogs
}