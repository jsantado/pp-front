import React from 'react'
import { Section } from '../../components'

function Home() {
  return (
    <div className='h-screen'>
      <Section isFirst className='bg-red dark:bg-red-500'>
        HOOOLA
      </Section>
      <Section className='bg-yellow dark:bg-yellow-500'>
        adddiiooos
      </Section>
      <Section className='bg-green dark:bg-green-500'>
        asdasdasd
      </Section>
    </div>
  )
}

export default Home