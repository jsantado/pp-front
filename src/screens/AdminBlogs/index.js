import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import Button from '../../components/Button';
import Form from './Form';

function AdminBlogs() {
    const [blog, setBlog] = useState({ en: { ps: [] }, es: { ps: [] }});
    const [selectedBlogLanguage, setSelectedBlogLanguage] = useState('en')
    const user = useSelector((state) => state.userReducer.user)

    useEffect(() => {
        const random = parseInt(Math.random() * 1000000000000, 10);
        setBlog({ ...blog, id: `${user._id}_${random}` })
    }, [])

    if (!blog?.id)
        return <div></div>
    
    const createBlog = () => {
        console.log("blog", blog)
    }

    return (
        <div className="w-full h-full flex justify-center items-center">
            <div className="w-2/4 dark:bg-main bg-white">
                <div className='w-full flex justify-between'>
                    <Button text="English" onClick={() => setSelectedBlogLanguage('en')} className={`w-2/5 ${selectedBlogLanguage === 'en' && 'dark:text-additional dark:border-additional text-secondary border-secondary'}`} />
                    <Button text="Spanish" onClick={() => setSelectedBlogLanguage('es')} className={`w-2/5 ${selectedBlogLanguage === 'es' && 'dark:text-additional dark:border-additional text-secondary border-secondary'}`} />
                </div>
                {selectedBlogLanguage === 'en' && <Form blog={blog.en} setBlog={(blogParam) => setBlog({ ...blog, en: blogParam })} />}
                {selectedBlogLanguage === 'es' && <Form blog={blog.es} setBlog={(blogParam) => setBlog({ ...blog, es: blogParam })} />}
                <div className='w-full flex justify-between mt-4'>
                    <Button text="Cancel" onClick={() => {}} className={`w-2/5`} />
                    <Button text="Create" onClick={createBlog} className={`w-2/5`} />
                </div>
            </div>
        </div>
    )
}

export default AdminBlogs