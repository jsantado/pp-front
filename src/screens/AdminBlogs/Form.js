import React from 'react'
import { FormRow, handleOnChange } from '../Login/utils'
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { FaTrashAlt } from "react-icons/fa";
import Button from '../../components/Button';


function Form({ blog, setBlog }) {
    const handleAddParragraph = () => {
        setBlog({ ...blog, ps: [...blog.ps, ""] })
    }
    const handleRemoveParragraph = (i) => {
        blog.ps.splice(i, 1)
        setBlog({ ...blog })
    }
    return (
        <>
            <FormRow field="title" onChange={(value) => setBlog(handleOnChange(blog, "title", value))} value={blog?.title || ""} />
            {
                blog?.ps?.length > 0 && blog.ps.map((p, index) => (
                    <div className={`w-full mb-4 justify-between items-center flex flex-col`}>
                        <label className={`w-full mb-2 h-8 capitalize text-main dark:text-secondary flex flex-row items-center justify-between`}>Parragraph {index + 1} <FaTrashAlt className="text-main dark:text-secondary dark:hover:text-additional hover:text-secondary text-1xl cursor-pointer" onClick={() => handleRemoveParragraph(index)} /></label>
                        <CKEditor
                            editor={ClassicEditor}
                            data={p}
                            onChange={(event, editor) => {
                                const data = editor.getData();
                                blog.ps[index] = data
                                setBlog(blog)
                            }}
                        />
                    </div>
                ))
            }
            <FormRow field="tags" text="Tags (Separate it by ;)" onChange={(value) => setBlog(handleOnChange(blog, "tags", value))} value={blog?.tags || ""} />
            <Button className='w-full' onClick={handleAddParragraph} text={'+'} />
        </>
    )
}

export default Form