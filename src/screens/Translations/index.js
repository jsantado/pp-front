import React, { useState } from 'react'
import { DictionaryLinguee, FormLinguee, PopUp } from './utils'
import Button from "../../components/Button"
import { createTranslation } from '../../redux/actions/translationsActions'
import { handleOnChange } from '../Login/utils'
import { useSelector } from 'react-redux'

function Translations() {
  const [isOpen, setIsOpen] = useState(false)
  const [translation, setTranslation] = useState({});
  const user = useSelector((state) => state.userReducer.user)

  const onClick = async () => {
    const res = await createTranslation(translation, user?.token)
    console.log(res)
  }

  return (
    <div>
      <Button
            disabled={''}
            text="+"
            onClick={() => setIsOpen(true)}
            className={`mb-2`}
          />
      <PopUp isOpen={isOpen} closedPopUp={() => setIsOpen(false)}>
        <div>
          <FormLinguee field='Key' onChange={(value) => setTranslation(handleOnChange(translation, 'key', value))}/>
          <FormLinguee field='Word' onChange={(value) => setTranslation(handleOnChange(translation, 'word', value))}/>
          <FormLinguee field='Traslate' onChange={(value) => setTranslation(handleOnChange(translation, 'translate', value))}/>
          <Button
            disabled={''}
            text="Add"
            onClick={onClick}
            className={`mb-2`}
          />
        </div>
      </PopUp>
      <div>
        <DictionaryLinguee />
      </div>
    </div>
  )
}

export default Translations