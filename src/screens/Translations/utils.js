import Button from "../../components/Button"
import Label from "../../components/Label"

export const FormLinguee = ({ field, type = 'text', onChange, text = null }) => (
    <div className={`pb-2 flex justify-center items-center`}>
        <label className={`pr-3 w-4/5 text-main dark:text-secondary`}>{text ? text : field}</label>
        <input
            className={`border-2 border-main rounded focus:border-secondary dark:focus:border-additional text-main p-1`}
            onChange={(event) => onChange(event.target.value)}
            type={type}
        />
    </div>
)

export const DictionaryLinguee = () => (
    <div className={`border-2 w-1/3 flex-col `}>
        <div className={`flex-row flex space-x-4 justify-between p-2 text-left`}>
            <Label className={`w-1/4`} value={'Key'} />
            <Label className={`w-1/4`} value={'ES'} />
            <Label className={`w-1/4`} value={'EN'} />
        </div>
        <div className={`flex-row flex space-x-4 justify-between p-2 text-left`}>
            <Label className={`w-1/4`} value={'-'} />
            <Label className={`w-1/4`} value={'Palabra'} />
            <Label className={`w-1/4`} value={'Word'} />
        </div>
    </div>
)

export const PopUp = ({ isOpen, children, closedPopUp }) => {
    if (!isOpen)
        return <></>
    return (
        <div className={`flex justify-center items-center absolute top-0 left-0 bg-main/50 h-screen w-screen`}>
            <div className={`flex justify-center items-center flex-col w-2/4`}>
                {children}
                <div className={`w-full flex justify-end items-center`}>
                    <Button text='close' onClick={closedPopUp} />
                </div>
            </div>
        </div>
    )
}