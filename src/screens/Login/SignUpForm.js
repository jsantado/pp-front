import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { Navigate } from 'react-router-dom';
import Button from '../../components/Button';
import { validateEmail } from '../../helpers/validators';
import { API_saveUser, setUser } from '../../redux/actions/userActions';
import { FormRow, handleOnChange, letterAndNumber, moreThan8, securePassword, SecurityPassword, symbols, upperAndLower } from './utils';

function SignUpForm() {
    const dispatch = useDispatch();
    const [payload, setPayload] = useState({});
    const [shouldRedirect, setShouldRedirect] = useState(false);
    const [error, setError] = useState(null);
    const isSecurePassword = moreThan8(payload) && upperAndLower(payload) && letterAndNumber(payload) && symbols(payload) && securePassword(payload)
    const isButtonDisabled = !payload?.email || !payload?.name || !payload?.surname || !payload?.password || !payload?.confirmPassword || (payload?.password !== payload?.confirmPassword || !validateEmail(payload?.email) || !isSecurePassword)
    
    const onClick = async () => {
        setError(null);
        const user = { ...payload }
        delete user?.confirmPassword
        const res = await API_saveUser(user)
        if (res?.status === 200) {
            const newUser = {
                ...res.data.user,
                token: res.data.token
            };
            delete newUser.password
            dispatch(setUser(newUser))
            setShouldRedirect(true)
        } else {
            setError("Oops, some error happened");
        }
        if (res?.response?.data?.errorMessage?.length > 0) {
            setError(res.response.data.errorMessage);
        }
    }

    if (shouldRedirect) return <Navigate to="/" />

    return (
        <div className={`w-full px-6 pb-6 mt-12 justify-center items-center flex flex-col`}>
            <FormRow field="email" type="email" onChange={(value) => setPayload(handleOnChange(payload, 'email', value))}  />
            <FormRow field="name" onChange={(value) => setPayload(handleOnChange(payload, 'name', value))}  />
            <FormRow field="surname" onChange={(value) => setPayload(handleOnChange(payload, 'surname', value))}  />
            <FormRow field="password" type="password" onChange={(value) => setPayload(handleOnChange(payload, 'password', value))}  />
            <SecurityPassword user={payload}/>
            <FormRow field="confirmPassword" type="password" text="Confirm Password" onChange={(value) => setPayload(handleOnChange(payload, 'confirmPassword', value))}  />
            {
                error?.length > 0 && <div className='text-error mb-4'>{error}</div>
            }
            <div className={`w-full flex justify-end`}>
                <Button
                    disabled={isButtonDisabled}
                    text="Sign Up"
                    onClick={onClick}
                />    
            </div>
        </div>
    )
}

export default SignUpForm