import React, { useState } from 'react'
import { Navigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { validateEmail } from '../../helpers/validators';
import Button from '../../components/Button';
import { API_login, setUser } from '../../redux/actions/userActions';
import { FormRow, handleOnChange } from './utils';

function LoginForm() {
  const dispatch = useDispatch();
  const [payload, setPayload] = useState({});
  const [shouldRedirect, setShouldRedirect] = useState(false);
  const [error, setError] = useState(null);
  const isButtonDisabled = !payload?.email || !payload?.password || !validateEmail(payload?.email)

  const onClick = async () => {
    setError(null);
    const res = await API_login(payload)
    if (res?.status === 200) {
      const user = {
        token: res?.data?.token,
        ...res?.data?.user
      }
      delete user.password
      dispatch(setUser(user))
      setShouldRedirect(true)
    } else {
      setError("Oops, some error happened");
    }
    if (res?.response?.data?.errorMessage?.length > 0) {
      setError(res.response.data.errorMessage);
    }
  }

  if (shouldRedirect) return <Navigate to="/" />

  return (
    <div className={`w-full px-6 pb-6 mt-12 justify-center items-center flex flex-col`}>
      <FormRow field="email" type="email" onChange={(value) => setPayload(handleOnChange(payload, 'email', value))} />
      <FormRow field="password" type="password" onChange={(value) => setPayload(handleOnChange(payload, 'password', value))} />
      {
        error?.length > 0 && <div className='text-error mb-4'>{error}</div>
      }
      <div className={`w-full flex justify-end`}>
        <Button
          disabled={isButtonDisabled}
          text="Log in"
          onClick={onClick}
        />
      </div>
    </div>
  )
}

export default LoginForm