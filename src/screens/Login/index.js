import React, { useEffect, useState } from 'react'
import LoginForm from './LoginForm';
import SignUpForm from './SignUpForm';

function Login() {
  const [selectedForm, setSelectedForm] = useState('login');

  return (
    <div className={`w-1/4 py-4 mt-10 justify-center items-center self-center flex flex-col`}>
      <div className={`w-1/3 justify-between flex`}>
        <button
          onClick={() => setSelectedForm('login')}
          className={`w-1/2 text-main dark:text-secondary hover:text-secondary dark:hover:text-additional`}
        >Login</button>
        <button
          onClick={() => setSelectedForm('signup')}
          className={`w-1/2 text-main dark:text-secondary hover:text-secondary dark:hover:text-additional`}
        >Register</button>
      </div>
      {
        selectedForm === 'login' ? <LoginForm /> : <SignUpForm />
      }
    </div>
  )
}

export default Login