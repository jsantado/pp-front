export const FormRow = ({field, type="text", onChange, text = null, value}) => (
    <div className={`w-full h-16 justify-between items-center flex flex-row`}>
        <label className={`w-2/5 mr-2 capitalize text-main dark:text-secondary`}>{text ? text : field}</label>
        <input
            className={`border-2 border-main rounded focus:border-secondary dark:focus:border-additional text-main w-3/4 p-1`}
            onChange={(evt) => onChange(evt.target.value)}
            type={type}
            value={value}
        />
    </div>
)

export const handleOnChange = (state, field, value) => ({...state, [field]: value})

const RequirementPassword = ({text, checked=false}) => {
    return(
        <div className={`w-full flex flex-row items-center`}>
            {
                checked ? <span className={`w-2 h-2 rounded-full border mr-2 bg-main border-main dark:bg-additional dark:border-additional `}/> : <span className={`w-2 h-2 rounded-full border mr-2`}/>
            }
            <span className={`text-sm ${checked ? 'text-main dark:text-additional' : 'text-secondary'}`}>
                {text}
            </span>
        </div>
    )
} 

export const moreThan8 = (user) => user?.password?.length > 7
export const upperAndLower = (user) => moreThan8(user) ? user.password !== user.password.toUpperCase() && user.password !== user.password.toLowerCase() : false 
export const letterAndNumber = (user) => moreThan8(user) ? /^([0-9]+[a-zA-Z]+|[a-zA-Z]+[0-9]+)[0-9a-zA-Z]/.test(user.password) : false
export const symbols = (user) => moreThan8(user) ? /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/.test(user.password) : false
export const secureName = (user) => moreThan8(user) && user?.name?.length > 0 ? user.password.toLowerCase().indexOf(user.name.toLowerCase()) === -1 : true
export const secureSurname = (user) => moreThan8(user) && user?.surname?.length > 0 ? user.password.toLowerCase().indexOf(user.surname.toLowerCase()) === -1 : true
export const securePassword = (user) => moreThan8(user) ? secureName(user) && secureSurname(user) : false

export const SecurityPassword = ({user}) => (
    <div className={`w-full`}>
        <RequirementPassword checked={moreThan8(user)} text="More than 8 characters "/>
        <RequirementPassword checked={upperAndLower(user)} text="Uppercase and lowercase letter"/>
        <RequirementPassword checked={letterAndNumber(user)} text="Contain letter and number" />
        <RequirementPassword checked={symbols(user)} text="Containing symbols"/> 
        <RequirementPassword checked={securePassword(user)} text="Secure password" /> 
    </div>
)

