import React from 'react'

function Section({ children, className = '', isFirst = false }) {
  return (
    <div className={`${isFirst ? 'h-full' : 'h-screen'} ${className}`}>
        {children}
    </div>
  )
}

export default Section