import React from 'react'

function Label({
    disabled = false,
    className = '',
    value
}) {
  return (
    <label
        type='text'
        className={`text-main dark:text-secondary ${className}`}
        disabled={disabled}
    >
      {value}
    </label>
  )
}

export default Label