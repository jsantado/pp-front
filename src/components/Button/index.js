import React from 'react'

function Button({
    disabled = false,
    text,
    className = '',
    onClick = () => {}
}) {
  return (
    <input
        type="button"
        className={`
            w-36 h-8 border-main border-2 rounded hover:border-secondary hover:text-secondary cursor-pointer
            dark:text-secondary dark:border-secondary dark:hover:text-additional dark:hover:border-additional
            ${className}
            disabled:text-disabled 
            disabled:border-disabled
            disabled:cursor-no-drop	

            dark:disabled:cursor-no-drop	
            dark:disabled:text-disabled
            dark:disabled:border-disabled
        `}
        onClick={onClick}
        disabled={disabled}
        value={text}
    />
  )
}

export default Button