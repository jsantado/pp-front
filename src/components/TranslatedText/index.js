import React from 'react'
import { useSelector } from 'react-redux'
import { getTranslation } from '../../redux/actions/translationsActions'

function TranslatedText({ name }) {
  const selectedLanguage = useSelector((state) => state.configReducer.selectedLanguage)
  const translations = useSelector((state) => state.translations.translations)

  if (!name) return <span>Translation name is required</span>

  const foundTranslation = getTranslation(translations, name);

  if (!foundTranslation?.length) return <span>Translation with name {name} does not exist</span>

  if (!foundTranslation[0].labels || !foundTranslation[0].labels[selectedLanguage])
    return <span>Translation is not filled on {selectedLanguage} language</span>

  return (
    <div>{foundTranslation[0].labels[selectedLanguage]}</div>
  )
}

export default TranslatedText