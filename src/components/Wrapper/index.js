import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';
import { checkToken } from '../../redux/actions/authActions';
import Navigation from '../Navigation'

function Wrapper({ children }) {
  const [shouldGoLogin, setShouldGoLogin] = useState(false);
  const [shouldGoHome, setShouldGoHome] = useState(false);
  const [loading, setLoading] = useState(false);
  const user = useSelector((state) => state.userReducer.user)
  const route = window?.location?.pathname;


  useEffect(() => {
    setShouldGoLogin(false)
    setShouldGoHome(false)
    setLoading(true)
    async function checkTokenHandler() {
      const res = await checkToken(user?.token);

      if (route !== '/login' && (res?.response?.status === 500 || res?.response?.status === 401))
        setShouldGoLogin(true)
      if (route === '/login' && res?.status === 200)
        setShouldGoHome(true)
    }
    
    // if (route !== '/' && route !== '/login') {
    //   if (user?.token)
    //     checkTokenHandler();
    //   else
    //     setShouldGoHome(true);
    // }
    // if (route === '/login')
    //   checkTokenHandler();

      setLoading(false)
  }, [route])

  if (shouldGoLogin) {
    return <Navigate to="/login" />
  }

  if (shouldGoHome) {
    return <Navigate to="/" />
  }

  return (
    <div className={`w-screen flex flex-col justify-start item-start h-full dark:bg-main`}>
      <Navigation />
      {
        !loading && children
      }
    </div>
  )
}

export default Wrapper