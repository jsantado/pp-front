import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import ThemeToggle from '../ThemeToggle';
import NavLink from './NavLink';

const loggedMenu = (openUserMenu, setOpenUserMenu) => (
    <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
        <div className="ml-3 relative">
            <div>
                <button type="button" className={`bg-gray-800 flex text-sm rounded-full ${openUserMenu ? 'outline-none ring-2 ring-offset-2 ring-offset-gray-800 ring-white' : ''}`} id="user-menu-button" aria-expanded="false" aria-haspopup="true" onClick={() => setOpenUserMenu(!openUserMenu)}>
                    <img className="h-8 w-8 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" />
                </button>
            </div>
            <div className={`origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none ${openUserMenu ? '' : 'hidden'}`} role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabindex="-1">
                <a href="#" className={`block px-4 py-2 text-sm`} role="menuitem" tabindex="-1">Settings</a>
                <a href="#" className={`block px-4 py-2 text-sm`} role="menuitem" tabindex="-1">Sign out</a>
            </div>
        </div>
    </div>
)

const unloggedMenu = () => (
    <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
        <div className="ml-3 relative">
            <div>
                <Link to="/login" className={`bg-gray-800 flex text-sm rounded-full cursor-pointer`} aria-expanded="false" aria-haspopup="true">
                    <img className="h-8 w-8 rounded-full" src="https://media.istockphoto.com/vectors/avatar-5-vector-id1131164548?k=20&m=1131164548&s=612x612&w=0&h=ODVFrdVqpWMNA1_uAHX_WJu2Xj3HLikEnbof6M_lccA=" alt="" />
                </Link>
            </div>
        </div>
    </div>
)

function Navigation() {
    const dispatch = useDispatch();
    const [openUserMenu, setOpenUserMenu] = useState(false);
    const user = useSelector((state) => state.userReducer.user);

    return (
        <nav className={`p-2 sm:px-6 lg:px-8 flex items-center justify-between relative w-full`}>
            <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                <div className="hidden sm:block sm:ml-6">
                    <div className="flex space-x-4">
                        <img className={`h-8 w-8 rounded-full hover:outline-none cursor-pointer mt-1 `} src="logo.jpg" />
                        <NavLink link="/blog" text="Blog" />
                        <NavLink link="/about-me" text="About me" />
                        <NavLink link="/admin/translations" text="Translations" />
                        <NavLink link="/admin/blogs" text="Admin Blogs" />
                    </div>
                </div>
            </div>
            {
                user?.id ? (
                    loggedMenu(openUserMenu, setOpenUserMenu)
                ) : (
                    unloggedMenu()
                )
            }
            <ThemeToggle />
        </nav>
    )
}

export default Navigation