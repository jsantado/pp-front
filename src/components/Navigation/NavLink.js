import React from 'react'
import { Link } from 'react-router-dom'

function NavLink({ link, text }) {
  return (
    <Link
        to={link}
        className={`
            h-10 text-main
            flex justify-center items-center px-6
            hover:text-secondary
            dark:text-secondary dark:hover:text-additional
        `}
    >
        {text}
    </Link>
  )
}

export default NavLink