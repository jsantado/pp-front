import * as types from '../actions/actionTypes';

const initialState = {
    selectedLanguage: "es"
};

export default function reducer(state = initialState, action) {
    let newState;
    switch (action.type) {
    case types.SET_SELECTED_LANGUAGE:
        newState = {
            ...state,
            selectedLanguage: action.selectedLanguage,
        };
        return newState;

    default:
        return state;
    }
}