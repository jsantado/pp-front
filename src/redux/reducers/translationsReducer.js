import * as types from '../actions/actionTypes';

const initialState = {
    translations: []
};

export default function reducer(state = initialState, action) {
    let newState;
    switch (action.type) {
    case types.SET_TRANSLATIONS:
        newState = {
            ...state,
            translations: action.translations,
        };
        return newState;

    default:
        return state;
    }
}