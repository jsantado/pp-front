import * as types from '../actions/actionTypes';

const initialState = {
    user: {}
};

export default function reducer(state = initialState, action) {
    let newState;
    switch (action.type) {
    case types.SET_USER:
        newState = {
            ...state,
            user: action.user,
        };
        return newState;

    case types.LOGOUT:
        newState = initialState;
        return newState;

    default:
        return state;
    }
}