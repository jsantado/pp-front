import userReducer from './userReducer';
import configReducer from './configReducer';

export default {
    userReducer,
    configReducer
};