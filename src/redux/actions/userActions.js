import axios from 'axios';
import * as types from './actionTypes';
const { BASE_URL = "http://localhost:8000" } = process.env;

export function logout() {
    return { type: types.LOGOUT };
}

export function setUser(user) {
    return { type: types.SET_USER, user };
}

export function API_saveUser(user) {
    return axios.post(`${BASE_URL}/auth/signup`, {user})
      .then(res => res)
      .catch((err) => err)
}

export function API_login(user) {
    return axios.post(`${BASE_URL}/auth/login`, user)
      .then(res => res)
      .catch((err) => err)
}