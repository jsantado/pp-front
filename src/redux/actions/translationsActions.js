import axios from 'axios';
import * as types from './actionTypes';
const { BASE_URL = "http://localhost:8000" } = process.env;

export function setTranslations(translations) {
    return { type: types.SET_TRANSLATIONS, translations };
}

export function getTranslations(name = '') {
    return axios.get(`${BASE_URL}/translations/search/${name}`)
        .then(res => res)
        .catch((err) => err)
}

export async function getTranslation(translations, name) {
    const foundInRedux = translations.filter((translation) => translation.name.toLowerCase() === name.toLowerCase())
    if (foundInRedux) return foundInRedux
    
    const res = getTranslations(name)
    if (res)  return res

    return null
}

export function createTranslation(translation, token = "") {
    return axios.post(`${BASE_URL}/translations/`, {translation},{ headers: { 'Authorization': `Bearer ${token}` }})
      .then(res => res)
      .catch((err) => err)
}