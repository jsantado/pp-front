import * as types from './actionTypes';

export function setSelectedLanguage(selectedLanguage) {
    return { type: types.SET_SELECTED_LANGUAGE, selectedLanguage };
}