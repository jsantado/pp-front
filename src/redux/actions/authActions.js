import axios from 'axios';
const { BASE_URL = "http://localhost:8000" } = process.env;

export const checkToken = (token = "") => {
    return axios.get(`${BASE_URL}/auth/secure`, { headers: { 'Authorization': `Bearer ${token}` }})
      .then(res => res)
      .catch((err) => err)
}